# whoami

This repository contains notifications of any changes made to my online identity. All commits to this repository should be signed by my PGP key. Moreover, all files will have a corresponsing `*.minisig` file containing a cryptographic signature to be verified as well. This way, integrity and authenticity can be ensured via 2 separate signing/verification mechanisms: PGP and Minisign (Note: Minisign signatures can also be verified via BSD's Signify).

The signing key used in commits should be:

(Minisign)
```
untrusted comment: minisign public key 73C675F1EAF79E80
RWSAnvfq8XXGcw5iUd2+q7OWwlITbIKkp5lUPKR3haFhdIWDdXFf1Rla
```

(PGP)
```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYKeDLhYJKwYBBAHaRw8BAQdAz9yv03vm0IzXJGuDuxws4/MZkVHZ3eWIqPDH
E2UTvoO0pU5vYm9keVNwZWNpYWwgKENvcnJlc3BvbmRzIHRvIG1pbmlzaWduIHB1
YmxpYyBrZXkgNzNDNjc1RjFFQUY3OUU4MCBbUldTQW52ZnE4WFhHY3c1aVVkMitx
N09Xd2xJVGJJS2twNWxVUEtSM2hhRmhkSVdEZFhGZjFSbGFdKSA8dGhlaGVhZGxl
c3NzZXJwZW50c2VjQHByb3Rvbm1haWwuY29tPokCJQQTFgoBzQIbAwULCQgHAgMi
AgEGFQoJCAsCBBYCAwECHgcCF4AWIQQpdMEXW/geRrxImDBtIanZ9HzByAUCYN4P
zKAUgAAAAAASAIVwcm9vZkBtZXRhY29kZS5iaXptYXRyaXg6dS9AbmVvYmFiYmE6
ZW52cy5uZXQ/b3JnLmtleW94aWRlLnI9IWRCZlFaeENvR1ZtU1R1amZpdjptYXRy
aXgub3JnJm9yZy5rZXlveGlkZS5lPSQ5VG1iUllZN0VxR0hzeEZCWm92QzJOV1Ja
anNua1hWdmp6UTd4b2FmOEVJSRSAAAAAABIALnByb29mQG1ldGFjb2RlLmJpemh0
dHBzOi8vZ2l0LmVudnMubmV0L05vYm9keVNwZWNpYWwvZ2l0ZWFfcHJvb2alFIAA
AAAAEgCKcHJvb2ZAbWV0YWNvZGUuYml6bWF0cml4OnUvQGJhYmJhMjc6YXJ0ZW1p
c2xlbmEuZXU/b3JnLmtleW94aWRlLnI9IWRCZlFaeENvR1ZtU1R1amZpdjptYXRy
aXgub3JnJm9yZy5rZXlveGlkZS5lPSQ3ZHhmWXBpS2JOUUpXYlNFeVRlVGZjWmNS
Q25YdG0yV1NzZ3VJWGx1OW1jAAoJEG0hqdn0fMHI0/QBALSrc3y1KFl0Xit8hRs6
ywphIHVPu6b9J+ZQrBJy6o0fAP9ByIMrQI5jdfTxzfG2lc0GLzkEu6a+IpiOi0ir
MNr0BLg4BGCngy4SCisGAQQBl1UBBQEBB0DaycllNaf/4/tUMG9nhylHisLDm+xc
QAwPMpoDBp2MHAMBCAeIeAQYFgoAIBYhBCl0wRdb+B5GvEiYMG0hqdn0fMHIBQJg
p4MuAhsMAAoJEG0hqdn0fMHIMtABAJMCiyXmek4eg6kb7GJrwj3B4Veeebk+CTT0
P32IUrfHAQCbWBkXzndeSlx9IuBxGoj6Xi6fxbh2qbtuDcpg2U3IAA==
=NGR9
-----END PGP PUBLIC KEY BLOCK-----
```
Fingerprint `2974C1175BF81E46BC4898306D21A9D9F47CC1C8`

Any changes made to the signing keys used should be mentioned in a message signed by *both* keys (even if the change is only to a single key). Anything signed by only a single key, or not signed at all, should not be trusted. Note that git commit signing counts as being signed by my PGP key (assuming my PGP key was the key used to sign the commit. Commits signed by different keys should not be trusted.).

My KeyOxide can be found at the following URL:
https://keyoxide.org/2974C1175BF81E46BC4898306D21A9D9F47CC1C8

Or via KeyOxide's CLI tool:
`keyoxide verify hkp:theheadlessserpentsec@protonmail.com`
